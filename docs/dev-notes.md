Development Notes
=================

## Touch Events
Touch gestures support is supposed to be moved into the core Godot input system in 4.0, but as of 4b10, it's not there yet. If we end up having to implement those gestures by hand, those solutions would be good starting points.

Swipes:
* https://github.com/GDQuest/godot-power-pitch/tree/master/godot-3-presentation
* https://github.com/aimforbigfoot/NAD-LAB-Godot-Projects/tree/master/Gestures

Long press:
*  https://www.reddit.com/r/godot/comments/iuj9tl/long_click_button/

Detected gesture should emit Actions:
* https://docs.godotengine.org/en/latest/tutorials/inputs/inputevent.html#actions
