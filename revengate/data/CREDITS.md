Lead coding: Yannick Gingras

Additionnal coding: 
- Nicole Schwartz
- Ryan Prior
- Steve Smith
- Josephine Maya Simple
- Victor Hunt
- Chris J. Wallace
- Angel Hudgins
- JT Wright
- Rodrigo Espinosa de los Monteros

2D tiles: CC0, released by the Craw Stone Soup team
https://opengameart.org/content/dungeon-crawl-32x32-tiles
https://github.com/crawl/tiles/tree/master/releases

Bestiary and NPC images:
- Le Grand Salapou, Éguis, Large Automata: CC-BY-SA by Jonathan Timmons, 2021
- Pridout Fauchève, Sulat Tiger, Sahwakoon: CC-BY by Clara Baltoré Pooter, 2021
- Henry Bessemer, Nochorts, Sentry Scarabs: CC-BY by Tuyên Đặng, 2021
- Labras, Pacherr, Phantruch (lesser and greater), Gleugt, Desert Centipede, Algerian Giant Locust, Cherub, Yarohu: Creative Commons Attribution 4.0 International License by Jason Teves, 2021

Splash Screen:
- CC-BY-SA, Zanya Fernández Rodríguez

kalimati.ttf:
- Copyright Sanir Karmacharya, released under GNU General Public License v2 or later
- this font is part of fonts-deva-extra

Symbola_hint.ttf:
- Copyright (C) 2007-2015 George Douros
- "Fonts are free for any use; they may be opened, edited, modified, regenerated, packaged and redistributed."

Shield outline on the launch icon: 
Public Domain by OpenClipart
https://freesvg.org/vector-drawing-of-blank-silver-shield

Icons in assets/opencliparts: 
- CC0 by the Open Cliparts project. More info at http://www.openclipart.org

Combat Sound Effects:
CC0 by Ben (artisticdude)
https://opengameart.org/content/rpg-sound-pack

Additionnal sound effects CC-BY-SA Yannick Gingras:
- end screens sound effects
- hammer-01
- stick-01
- ethereal-01, 02
- razor
- explosion-01

Particle sprites:
CC0 by Kenney
https://www.kenney.nl/

The historical map of Lyon in 1855 is Public Domain, digitize by Archives Municipales de Lyon.
