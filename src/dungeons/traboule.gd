# Copyright © 2023 Yannick Gingras <ygingras@ygingras.net> and contributors

# This file is part of Revengate.

# Revengate is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# Revengate is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with Revengate.  If not, see <https://www.gnu.org/licenses/>.

class_name Traboule extends Dungeon

const FIRST_MAZE = 6
const ALL_MAZES = 13

func dungeon_for_loc(world_loc:Vector3i):
	## Return the name of the dungeon where `world_loc` belongs or null is it's part of the current dungeon
	if world_loc.z >= 0:
		return "LyonSurface"
	return null

func make_builder(board, rect):
	## Return a new builder configure for the style of the current dungeon.
	var builder = BoardBuilder.new(board, rect)
	builder.floor_terrain = "floor-rough"
	builder.wall_terrain = "wall-old"	
	return builder

func fill_new_board(builder, depth, world_loc, size):
	## put the main geometry on a freshly created board, except for connectors
	var outer_rect = Rect2i(Vector2i.ZERO, size)

	if _lvl_is_maze(depth, world_loc):
		assert(builder.rect.size % 2 == Vector2i.ONE, "maze levels should have odd sizes")
		builder.paint_rect(outer_rect, builder.wall_terrain)
		var inner_rect = Rect2i(outer_rect.position+Vector2i.ONE, outer_rect.size-Vector2i.ONE*2)
		var biases = _maze_biases(depth)
		builder.gen_maze(inner_rect, biases)
	else:
		builder.paint_rect(outer_rect, builder.clear_terrain)
		builder.gen_rooms(randi_range(3, 6))

func _lvl_is_maze(depth:int, world_loc:Vector3i):
	## Return whether the next board be a maze?
	if _loc_elev(world_loc) == tunneling_elevation:
		return true
	else:
		return Rand.linear_prob_test(depth, FIRST_MAZE-1, ALL_MAZES)

func _maze_biases(depth:int):
	## Return the bias params for a maze generated at a given depth
	var easy_depth = FIRST_MAZE
	var hard_depth = ALL_MAZES
	var easy_reconnect = 0.7
	var hard_reconnect = 0.3
	var diff_slope = (hard_reconnect - easy_reconnect) / (hard_depth - easy_depth)
	var diff_steps = (clamp(depth, easy_depth, hard_depth) - easy_depth)
	var reconnect = diff_steps * diff_slope + easy_reconnect
	return {"twistiness": 0.3, "branching": 0.3, "reconnect": reconnect}

func add_connectors(builder:BoardBuilder, neighbors):
	## place stairs and other cross-board connectors on a board
	var board = builder.board as RevBoard
	var coord:Vector2i	
	for rec in neighbors:
		var region = _region_for_loc(board.world_loc, rec.world_loc)
		var terrain = _neighbor_connector_terrain(board.world_loc, rec.world_loc)
		if terrain == "gateway" and region != Consts.REG_CENTER and not builder.has_rooms():
			coord = Rand.coord_on_rect_perim(builder.rect, region)
		else:
			coord = builder.random_coord_in_region(region, board.is_floor)
		builder.paint_cells([coord], terrain)
		board.set_cell_rec(coord, "conn_target", rec)
